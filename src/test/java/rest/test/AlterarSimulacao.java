package rest.test;


import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.Random;  

import org.junit.Test;

public class AlterarSimulacao {
	String host = "http://localhost:8080";
	int valor = 1000;
	int parcelas = 2;

	@Test
	public void PutSimulacao() {
			given()
				.contentType("application/json")
				.body("{\r\n" + 
						"  \"nome\": \"Scooby\",\r\n" + 
						"  \"cpf\": 66414919004,\r\n" +
						"  \"email\": \"scoobyd@me.com\",\r\n" + 
						"  \"valor\": 3000,\r\n" + 
						"  \"parcelas\": 3,\r\n" + 
						"  \"seguro\": false\r\n" + 
						"}")
			.when()
				.put(host + "/api/v1/simulacoes/66414919004")
			.then()
				.statusCode(201)
				;
	}
	
	@Test
	public void PutSimulacoesCPFRepetido() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"Scooby\",\r\n" + 
					"  \"cpf\": 66414919004,\r\n" + 
					"  \"email\": \"scoobyd@me.com\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.put(host + "/api/v1/simulacoes/17822386034")
		.then()
			.statusCode(400)
			.body("mensagem", is("CPF j� existente"));
			;
	}
	
	@Test
	public void PutSimulacoesErroNome() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": 3000,\r\n" + 
					"  \"cpf\": 66414919004,\r\n" +
					"  \"email\": \"scoobyd@me.com\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.put(host + "/api/v1/simulacoes/66414919004")
		.then()
			.statusCode(400)
			;
	}
	@Test
	public void PutSimulacoesErroEmail() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": 66414919004,\r\n" +
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.put(host + "/api/v1/simulacoes/66414919004")
		.then()
			.statusCode(400)
			;
	}
	
	@Test
	public void PutSimulacoesComSeguro() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": 66414919004,\r\n" +
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": true\r\n" + 
					"}")
		.when()
			.put(host + "/api/v1/simulacoes/66414919004")
		.then()
			.statusCode(400)
			;
	}
	
	@Test
	public void PutSimulacoesSemSeguro() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": 66414919004,\r\n" + 
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.put(host + "/api/v1/simulacoes/66414919004")
		.then()
			.statusCode(400)
			;
	}

	int newCPF () {
		Random random = new Random();   
		int newCpf = random.nextInt(999999999);
		return newCpf;
	}
	
}
	



	