package rest.test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;


public class Restricoes {

	String host = "http://localhost:8080";
	String[] cpf_invalido = { "97093236014", "60094146012", "84809766080", "62648716050", "26276298085", "01317496094",
			"55856777050", "19626829001", "24094592008", "58063164083" };
	String[] cpf_valido = { "44332912004", "40966355024", "55145923082" };
	
	String mensagem = ("O CPF " + cpf_invalido[0] + " tem problema");
	

	@Test
	public void CPFComRestricoes() {
		for (int i = 0; i < cpf_invalido.length; i++) {
			String mensagem = ("O CPF " + cpf_invalido[i] + " tem problema");

			given()
			.when()
				.get(host + "/api/v1/restricoes/" + cpf_invalido[i])
			.then()
				.statusCode(200).body("mensagem", is(mensagem));
		}
	}
	
	@Test
	public void CPFSemRestricoes() {
		for (int i = 0; i < cpf_valido.length; i++) {
			given()
			.when()
				.get( host + "/api/v1/restricoes/" + cpf_valido[i])
			.then()
				.statusCode(204)
				;								
		}
	}
}