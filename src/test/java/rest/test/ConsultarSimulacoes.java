package rest.test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;


public class ConsultarSimulacoes {

	String host = "http://localhost:8080";
	int valor = 1000;
	int parcelas = 2;

	@Test
	public void GetSimulacoes() {
			given()
			.when()
				.get(host + "/api/v1/simulacoes")
			.then()
				.statusCode(200)
				;
	}
	
//	@Test
//	public void GetSimulacoesVazio() {
////			* Necessário  limpar banco
//			given()
//			.when()
//				.get(host + "/api/v1/simulacoes")
//			.then()
//				.statusCode(204)
//				;
//	}
	
	@Test
	public void GetSimulacoesCPF() {
			given()
			.when()
				.get(host + "/api/v1/simulacoes/17822386034")
			.then()
				.statusCode(200)
				.body("id", is(12))
				.body("nome", is("Deltrano"))
				.body("email", is("deltrano@gmail.com"))
				.body("valor", is(20000.0F))
				.body("parcelas", is(5))
				.body("seguro", is(false))
				.body("cpf", is("17822386034"))
				;
	}
	
	@Test
	public void GetSimulacoesCPFFalso() {
			given()
			.when()
				.get(host + "/api/v1/simulacoes/123123")
			.then()
				.statusCode(404)
				
				;
	}
	
}