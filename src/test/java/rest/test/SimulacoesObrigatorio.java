package rest.test;

import static io.restassured.RestAssured.*;
import java.util.Random;  
import org.junit.Test;



public class SimulacoesObrigatorio {

		String host = "http://localhost:8080";
		int valor = 1000;
		int parcelas = 2;

		@Test
		public void PostSimulacaoSemNome() {
				int newCpf = newCPF ();
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"cpf\": "
							+ newCpf
							+ ",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}
		
		@Test
		public void PostSimulacaoSemCPF() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}
		
		@Test
		public void PostSimulacaoSemEmail() {
				int newCpf = newCPF ();
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": "
							+ newCpf
							+ ",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}
		
		@Test
		public void PostSimulacaoSemValor() {
				int newCpf = newCPF ();
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": "
							+ newCpf
							+ ",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}
		
		@Test
		public void PostSimulacaoSemParcelas() {
				int newCpf = newCPF ();
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": "
							+ newCpf
							+ ",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" +  
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}

		@Test
		public void PostSimulacaoSemSeguro() {
				int newCpf = newCPF ();
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": "
							+ newCpf
							+ ",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"}")
				.when()
					.post(host + "/api/v1/simulacoes")
				.then()
					.statusCode(400)
					;
		}
		int newCPF () {
			Random random = new Random();   
			int newCpf = random.nextInt(999999999);
			return newCpf;
		}
		
		@Test
		public void PutSimulacaoSemNome() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"cpf\": 66414919004,\r\n" +
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}
		
		@Test
		public void PutSimulacaoSemCPF() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}
		
		@Test
		public void PutSimulacaoSemEmail() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": 66414919004,\r\n" +
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}
		
		@Test
		public void PutSimulacaoSemValor() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": 66414919004,\r\n" +
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}
		
		@Test
		public void PutSimulacaoSemParcelas() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": 66414919004,\r\n" +
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" +  
							"  \"seguro\": false\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}

		@Test
		public void PutSimulacaoSemSeguro() {
				given()
					.contentType("application/json")
					.body("{\r\n" + 
							"  \"nome\": \"Scooby\",\r\n" + 
							"  \"cpf\": 66414919004,\r\n" +
							"  \"email\": \"scoobyd@me.com\",\r\n" + 
							"  \"valor\": 3000,\r\n" + 
							"  \"parcelas\": 3,\r\n" + 
							"}")
				.when()
					.put(host + "/api/v1/simulacoes/66414919004")
				.then()
					.statusCode(200)
					;
		}
		
}