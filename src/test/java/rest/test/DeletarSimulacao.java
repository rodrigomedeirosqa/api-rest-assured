package rest.test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class DeletarSimulacao {

	String host = "http://localhost:8080";
	int valor = 1000;
	int parcelas = 2;
	
	@Test
	public void DeletaSimulacao() {
			given()
			.when()
				.delete(host + "/api/v1/simulacoes/66414919004")
			.then()
				.statusCode(204)
				;
	}
	
	@Test
	public void DeletaSimulacaoInvalido() {
			given()
			.when()
				.delete(host + "/api/v1/simulacoes/000111")
			.then()
				.statusCode(404)
				.body("mensagem", is("Simula��o n�o encontrada"));
				;
	}	
}

	