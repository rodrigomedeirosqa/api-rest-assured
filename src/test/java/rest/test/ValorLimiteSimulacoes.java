package rest.test;

import static io.restassured.RestAssured.*;
import java.util.Random;  
import org.junit.Test;




public class ValorLimiteSimulacoes {

	String host = "http://localhost:8080";
	int valor = 1000;
	int parcelas = 2;

	@Test
	public void ValorLimitePermitidoMenor() {
		MetodoSimulacao(10000.0, 2, 201);
	}
	
	@Test
	public void ValorLimitePermitidoMaior() {
		MetodoSimulacao(40000.0, 10, 201);
	}
	@Test
	public void ValorLimiteParcelasPermitidoMaior() {
		MetodoSimulacao(10000.0, 48, 201);
	}
	
	@Test
	public void ValorLimiteParcelasPermitidoMenor() {
		MetodoSimulacao(10000.0, 2, 201);
	}
	
	@Test
	public void ValorLimiteInvalidoMenor() {
		MetodoSimulacao(999.99, 2, 400);
	}
	
	@Test
	public void ValorLimiteInvalidoMaior() {
		MetodoSimulacao(40000.01, 2, 400);
	}
	
	@Test
	public void ValorLimiteParcelasInvalidoMaior() {
		MetodoSimulacao(10000.0, 49, 400);
	}
	
	@Test
	public void ValorLimiteParcelasInvalidoMenor() {
		MetodoSimulacao(10000.0, 1, 400);
	}
	
	@Test
	public void AlterarValorLimitePermitidoMenor() {
		PutMetodoSimulacao(10000.0, 2, 201);
	}
	
	@Test
	public void AlterarValorLimitePermitidoMaior() {
		PutMetodoSimulacao(40000.0, 10, 201);
	}
	@Test
	public void AlterarValorLimiteParcelasPermitidoMaior() {
		PutMetodoSimulacao(10000.0, 48, 201);
	}
	
	@Test
	public void AlterarValorLimiteParcelasPermitidoMenor() {
		PutMetodoSimulacao(10000.0, 2, 201);
	}
	
	@Test
	public void AlterarValorLimiteInvalidoMenor() {
		PutMetodoSimulacao(999.99, 2, 400);
	}
	
	@Test
	public void AlterarValorLimiteInvalidoMaior() {
		PutMetodoSimulacao(40000.01, 2, 400);
	}
	
	@Test
	public void AlterarValorLimiteParcelasInvalidoMaior() {
		PutMetodoSimulacao(10000.0, 49, 400);
	}
	
	@Test
	public void AlterarValorLimiteParcelasInvalidoMenor() {
		PutMetodoSimulacao(10000.0, 1, 400);
	}
	
	static void MetodoSimulacao(double valor, int parcelas, int statuscode) {
		String host = "http://localhost:8080";
		Random random = new Random();   
		int newCpf = random.nextInt(999999999);
		
		given()
		.contentType("application/json")
		.body("{\r\n" + 
				"  \"nome\": \"New CPF\",\r\n" + 
				"  \"cpf\": "
				+ newCpf
				+ ",\r\n" + 
				"  \"email\": \"new@me.com\",\r\n" + 
				"  \"valor\": "
				+ valor
				+ ",\r\n" + 
				"  \"parcelas\": "
				+ parcelas
				+ ",\r\n" + 
				"  \"seguro\": false\r\n" + 
				"}")
	.when()
		.post(host + "/api/v1/simulacoes")
	.then()
		.statusCode(statuscode)
		;
	}
	
	static void PutMetodoSimulacao(double valor, int parcelas, int statuscode) {
		String host = "http://localhost:8080";
		
		given()
		.contentType("application/json")
		.body("{\r\n" + 
				"  \"nome\": \"New CPF\",\r\n" + 
				"  \"cpf\": 66414919004,\r\n" +
				"  \"email\": \"new@me.com\",\r\n" + 
				"  \"valor\": "
				+ valor
				+ ",\r\n" + 
				"  \"parcelas\": "
				+ parcelas
				+ ",\r\n" + 
				"  \"seguro\": false\r\n" + 
				"}")
	.when()
		.put(host + "/api/v1/simulacoes/66414919004")
	.then()
		.statusCode(statuscode)
		;
	}
}
