package rest.test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import java.util.Random;  
import org.junit.Test;



public class Simulacoes {
	String host = "http://localhost:8080";
	int valor = 1000;
	int parcelas = 2;

	@Test
	public void PostSimulacoes() {
			int newCpf = newCPF ();
			given()
				.contentType("application/json")
				.body("{\r\n" + 
						"  \"nome\": \"Scooby\",\r\n" + 
						"  \"cpf\": "
						+ newCpf
						+ ",\r\n" + 
						"  \"email\": \"scoobyd@me.com\",\r\n" + 
						"  \"valor\": 3000,\r\n" + 
						"  \"parcelas\": 3,\r\n" + 
						"  \"seguro\": false\r\n" + 
						"}")
			.when()
				.post(host + "/api/v1/simulacoes")
			.then()
				.statusCode(201)
				;
	}
	
	@Test
	public void PostSimulacoesCPFRepetido() {
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"Scooby\",\r\n" + 
					"  \"cpf\": 66414919004,\r\n" + 
					"  \"email\": \"scoobyd@me.com\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(400)
			.body("mensagem", is("CPF j� existente"));
			;
	}
	
	@Test
	public void PostSimulacoesErroNome() {
		int newCpf = newCPF ();
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": 3000,\r\n" + 
					"  \"cpf\": " + newCpf + ",\r\n" + 
					"  \"email\": \"scoobyd@me.com\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(400)
			;
	}
	@Test
	public void PostSimulacoesErroEmail() {
		int newCpf = newCPF ();
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": " + newCpf + ",\r\n" + 
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(400)
			;
	}
	
	@Test
	public void PostSimulacoesComSeguro() {
		int newCpf = newCPF ();
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": " + newCpf + ",\r\n" + 
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": true\r\n" + 
					"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(400)
			;
	}
	
	@Test
	public void PostSimulacoesSemSeguro() {
		int newCpf = newCPF ();
		given()
			.contentType("application/json")
			.body("{\r\n" + 
					"  \"nome\": \"scooby\",\r\n" + 
					"  \"cpf\": " + newCpf + ",\r\n" + 
					"  \"email\": \"scooby\",\r\n" + 
					"  \"valor\": 3000,\r\n" + 
					"  \"parcelas\": 3,\r\n" + 
					"  \"seguro\": false\r\n" + 
					"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(400)
			;
	}

	int newCPF () {
		Random random = new Random();   
		int newCpf = random.nextInt(999999999);
		return newCpf;
	}
	
	static void MetodoSimulacao(String nome, double valor, int parcelas, int statuscode) {
		String host = "http://localhost:8080";
		Random random = new Random();   
		int newCpf = random.nextInt(999999999);

		given()
		.contentType("application/json")
		.body("{\r\n" + 
				"  \"nome\": "
				+ nome
				+ ",\r\n" + 
				"  \"cpf\": "
				+ newCpf
				+ ",\r\n" + 
				"  \"email\": \"new@me.com\",\r\n" + 
				"  \"valor\": "
				+ valor
				+ ",\r\n" + 
				"  \"parcelas\": "
				+ parcelas
				+ ",\r\n" + 
				"  \"seguro\": false\r\n" + 
				"}")
		.when()
			.post(host + "/api/v1/simulacoes")
		.then()
			.statusCode(statuscode)
			;
	}
	
}
	

