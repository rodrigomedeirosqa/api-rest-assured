
# API Teste 

Scripts de testes em Rest Assured.

## Configurações

* Instalar Java JDK
* Configurar Maven na variável de ambiente
* Configurar JAVA_HOME na variável de ambiente
* Instalar Eclipse IDE
* Iniciar aplicação com Sprint Boot


## Preparando Testes
1. Realizar execução da aplicação: `mvn clean spring-boot:run`
2. Abrir Eclipse IDE
3. Em File selecionar Open Project From File System
4. Selecionar Pasta ...
5. Concluir Importação

## Executando Testes
1. Em src/test/java
2. Selecionar pacote e executar com JUnit
